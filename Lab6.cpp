/****************
Nick Kriz
CPSC 1021, Section 3, F20
nkriz@clemson.edu
Elliot McMillan
*****************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
//All includes are required for use of certain C++ libraries used in the lab

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;
//Struct is declared with employee as the name of the type

bool name_order(const employee& lhs, const employee& rhs);
/*The purpose of this function is to take in the whole new aray and
return a value of true or false based on whether or not the left hand last
name is smaller than the right hand last name. It is then entered into
the sort function
*/
int myrandom (int i) { return rand()%i;}
/*The purpose of this function is to randomize for the random shuffle function.
This function retunrs a random integer using time as a seed.
*/

int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  int i = 0;

  employee empArr[10];
	//This initializes the employee array of 10 people

	employee *arrPoint = &empArr[10];
	//Creates a pointer that points to one past the end of the array

  while(i < 10){

  	cout << "What is the last name of the employee?" << endl;
  	cin >> empArr[i].lastName;

  	cout << "What is the first name of the employee?" << endl;
  	cin >> empArr[i].firstName;

  	cout << "What is their birth year?" << endl;
  	cin >> empArr[i].birthYear;

  	cout << "What is their hourly wage?" << endl;
  	cin >> empArr[i].hourlyWage;

  	i++;
  }
	/*This while statement will loop until 10 employees information has all been
	entered*/

  std::random_shuffle(empArr, arrPoint, myrandom);
	/*This is the random shuffle that allows us to input the entire employee
	array and the random function. This will shuffle the entire array which we
	will later use to create a new array.*/

  employee newArr[5] = {empArr[0], empArr[1], empArr[2], empArr[3], empArr[4]};
	//New array is created and assigned to the first 5 random employees

	arrPoint = &newArr[5];
	//Change the pointer from the end of the pervious array to the new end

	std::sort(newArr, arrPoint, name_order);
	/*The sort function takes in the new array plus the pointer so the entire
	thing can be captured. Additionally, it takes in the comparison parameter
	that determines what the order of the array will be*/

	for(int i = 0; i < 5; i++){
		string lastAndFirst = newArr[i].lastName + ", " + newArr[i].firstName;
		cout << right << setw(25) << lastAndFirst << endl;
		cout << setw(25) << right << newArr[i].birthYear << endl;
		cout << setw(25) << right << fixed << showpoint << setprecision(2);
		cout << newArr[i].hourlyWage << endl;
	}
	/*This prints out all the information using a for loop so that it does not
	go past the 5 employees. Also, setw is used for all of them as well as right
	which alligns everything to the right hand side. As for the wage, I included
	showpoint and set precision at 2 so the correct decimals appear for money*/




  return 0;
}

bool name_order(const employee& lhs, const employee& rhs) {
  if(lhs.lastName < rhs.lastName){
		return true;
	}
	else{
		return false;
	}
	/*Only needs two statements, if the last name of lhs is smaller, it's true.
	If it's not smaller, than it will return false to main.*/
}
